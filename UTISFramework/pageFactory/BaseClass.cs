﻿using log4net;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UTISFramework.pageFactory
{
    public class BaseClass
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected IWebDriver Driver { get; set; }

        public BaseClass (IWebDriver driver)
        {
            this.Driver = driver;
            PageFactory.InitElements(driver, this);
           // defaultTimeOutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["defaultTimeOutSeconds"]);
        }

        public void OpenURL(string url)
        {
            Console.WriteLine($"Open URL: {url} ");
            Driver.Navigate().GoToUrl(url);
            WaitForPageLoading();
        }

        public void Click(IWebElement element)
        {
            WaitForPageLoading();
            WaitForElementToLoad(element);
            element.Click();
        }

        public void ClickByJS(IWebElement element)
        {
            WaitForPageLoading();
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript("arguments[0].click();", element);
        }

        public void Type(IWebElement element, string value)
        {
            WaitForElementToLoad(element);
            element.SendKeys(OpenQA.Selenium.Keys.Control + 'a');
            element.SendKeys(value);
        }

        public void Check(IWebElement element)
        {
            if (!element.Selected)
            {
                Click(element);
            }
            else
            {
                Console.WriteLine("Element is already checked.");
            }
        }

        public void UnCheck(IWebElement element)
        {
            if (element.Selected)
            {
                Click(element);
            }
            else
            {
                Console.WriteLine("Element is already Unchecked.");
            }
        }

        public void SelectDropdownByValue(IWebElement element, string value)
        {
            var selectElement = new SelectElement(element);
            selectElement.SelectByValue(value);
            Console.WriteLine($"Selected Value is : {selectElement.SelectedOption.Text}");
        }

        public void SelectDropdownByIndex(IWebElement element, int value)
        {
            var selectElement = new SelectElement(element);
            selectElement.SelectByIndex(value);
            Console.WriteLine($"Selected Value is : {selectElement.SelectedOption.Text}");
        }

        public void SelectDropdownByVisibleText(IWebElement element, string value)
        {
            var selectElement = new SelectElement(element);
            selectElement.SelectByText(value);
            Console.WriteLine($"Selected Value is : {selectElement.SelectedOption.Text}");
        }



        public void ScrollToElement(IWebElement element)
        {
            Actions actions = new Actions(Driver);
            actions.MoveToElement(element);
            actions.Perform();
        }

        public void ScrollToElementByJS(IWebElement element, bool alignToTop = true)
        {
            string alignToTopString = alignToTop.ToString().ToLower();
            string jScript = "arguments[0].scrollIntoView(" + alignToTopString + ");";
            IJavaScriptExecutor executor = (IJavaScriptExecutor)Driver;
            executor.ExecuteScript(jScript, element);
            Thread.Sleep(500);
        }

        public void SwitchToIframe(IWebElement element)
        {
            Driver.SwitchTo().Frame(element);
            Console.WriteLine($"Switched To IFrame Successfully");
        }

        public void SwitchToDefaultContent()
        {
            Driver.SwitchTo().DefaultContent();
            Console.WriteLine($"Switched To Default Content Successfully");
        }

        public void SwitchToNewWindow()
        {
            IReadOnlyCollection<string> allWindowHandles = Driver.WindowHandles;
            Driver.SwitchTo().Window(allWindowHandles.Last());
            Console.WriteLine($"Switched To latest window Successfully");
        }

        public void SwitchToParentWindow()
        {
            IReadOnlyCollection<string> allWindowHandles = Driver.WindowHandles;
            Driver.SwitchTo().Window(allWindowHandles.First());
            Console.WriteLine($"Switched To First window Successfully");
        }

        public void VerifyText(IWebElement element, string value)
        {
            var actText = ExtractTextFromElement(element);
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.AreEqualIgnoringCase(expText, actText, $"Verification of text failed");
        }

        public void VerifyTitle(string value)
        {
            var actText = Driver.Title;
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.AreEqualIgnoringCase(expText, actText, $"Verification of Title failed");
        }

        public void VerifyTitleContains(string value)
        {
            var actText = Driver.Title;
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.Contains(expText, actText, $"Verification of Title Contains failed");
        }

        public void VerifyUrl(string value)
        {
            var actText = Driver.Url;
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.AreEqualIgnoringCase(expText, actText, $"Verification of URL failed");
        }

        public void VerifyUrlContains(string value)
        {
            var actText = Driver.Url;
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.Contains(expText, actText, $"Verification of URL Contains failed");
        }

        public void VerifyTextContains(IWebElement element, string value)
        {
            var actText = ExtractTextFromElement(element);
            var expText = value;
            Console.WriteLine($"Expected Value: {expText}");
            Console.WriteLine($"Actual Value: {actText}");
            StringAssert.Contains(expText, actText, $"Verification of text contains failed");
        }

        public string ExtractTextFromElement(IWebElement webElement)
        {

            string textFromElement;
            if (webElement.TagName.Equals("input", StringComparison.OrdinalIgnoreCase))
            {
                textFromElement = webElement.GetAttribute("value").Trim();
            }
            else if (webElement.TagName.Equals("select", StringComparison.OrdinalIgnoreCase))
            {
                SelectElement select = new SelectElement(webElement);
                textFromElement = select.SelectedOption.Text.Trim();
            }
            else
            {
                textFromElement = webElement.Text.Trim();
            }
            return textFromElement;
        }

        public IList<string> GetAvailableOptionsFromDropdown(IWebElement element)
        {
            IList<string> availableOptions = new List<string>();
            var selectElement = new SelectElement(element);
            IList<IWebElement> options = selectElement.Options;
            foreach (IWebElement option in options)
            {
                availableOptions.Add(option.GetAttribute("innerText"));
            }
            return availableOptions;
        }


        public string WaitForPageLoading(int Timeout = 60)
        {
            WaitForJQueryLoading();
            try
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(Timeout));
                wait.Until<bool>((d) =>
                {
                    try
                    {
                        string test = ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").ToString();
                        if (test.Equals("complete"))
                            return true;
                        return false;
                    }
                    catch
                    {
                        return false;
                    }
                });
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string WaitForJQueryLoading(int Timeout = 60)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(Timeout));
                wait.Until<bool>((d) =>
                {
                    try
                    {
                        var test = (bool)((IJavaScriptExecutor)Driver).ExecuteScript("return jQuery.active == 0");
                        if (test)
                            return true;
                        return false;
                    }
                    catch
                    {
                        return true;
                    }
                });
                return "OK";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public bool WaitForElementToLoad(IWebElement element, int maxWaitTime = 60)
        {
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(0);
            bool isElementLoaded = false;
            int waitTime = 0;
            while (waitTime < maxWaitTime)
            {
                if (IsElementDisplayedAndEnabled(element))
                {
                    isElementLoaded = true;
                    break;
                }
                Thread.Sleep(500);
                waitTime++;
            }
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            return isElementLoaded;
        }

        public bool IsElementDisplayedAndEnabled(IWebElement element)
        {
            bool isEnabledAndDisplayed = false;
            try
            {
                isEnabledAndDisplayed = (element.Displayed && element.Enabled);
                return isEnabledAndDisplayed;
            }
            catch (Exception)
            {
                return isEnabledAndDisplayed;
            }
        }

        public bool IsElementDisabled(IWebElement element)
        {
            bool isEnabledAndDisplayed = false;
            try
            {
                isEnabledAndDisplayed = (element.Displayed && element.Enabled);
                return !isEnabledAndDisplayed;
            }
            catch (Exception)
            {
                return isEnabledAndDisplayed;
            }
        }

        public bool IsElementDisplayed(IWebElement element)
        {
            bool isDisplayed = false;
            try
            {
                isDisplayed = (element.Displayed);
                return isDisplayed;
            }
            catch (Exception e)
            {
                return isDisplayed;
            }
        }

        public void TakeScreenshot(IWebDriver driver, bool wholePage = true)
        {
            var logsDir = ConfigurationManager.AppSettings["logsDir"] + "\\";
            logsDir = AppDomain.CurrentDomain.BaseDirectory + "\\" + logsDir;
            string saveLocation = @logsDir;
            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss-ffff");
            if (wholePage)
            {
                Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(saveLocation + timestamp + "_ViewPort.png", ScreenshotImageFormat.Png);
                Bitmap fullPageSS = TakeFullPageScreenshot(driver);
                fullPageSS.Save(saveLocation + timestamp + "_FullPage.png", ImageFormat.Png);
              
            }
            else
            {
                Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
                screenshot.SaveAsFile(saveLocation + timestamp + "_ViewPort.png", ScreenshotImageFormat.Png);
                Bitmap fullPageSS = TakeFullPageScreenshot(driver);
                fullPageSS.Save(saveLocation + timestamp + "_FullPage.png", ImageFormat.Png);
            }

            Console.WriteLine("View Port Screenshot: {0}", new Uri(saveLocation + timestamp + "_ViewPort.png"));
            Console.WriteLine("Full Page Screenshot: {0}", new Uri(saveLocation + timestamp + "_FullPage.png"));
        }

        public Bitmap TakeFullPageScreenshot(IWebDriver _driver)

        {
            Bitmap stitchedImage = null;
            try
            {
                long totalwidth1 = (long)((IJavaScriptExecutor)_driver).ExecuteScript("return document.body.offsetWidth");//documentElement.scrollWidth");
                long totalHeight1 = (long)((IJavaScriptExecutor)_driver).ExecuteScript("return  document.body.parentNode.scrollHeight");
                int totalWidth = (int)totalwidth1;
                int totalHeight = (int)totalHeight1;
                // Get the Size of the Viewport
                long viewportWidth1 = (long)((IJavaScriptExecutor)_driver).ExecuteScript("return document.body.clientWidth");//documentElement.scrollWidth");
                long viewportHeight1 = (long)((IJavaScriptExecutor)_driver).ExecuteScript("return window.innerHeight");//documentElement.scrollWidth");
                int viewportWidth = (int)viewportWidth1;
                int viewportHeight = (int)viewportHeight1;
                // Split the Screen in multiple Rectangles
                List<Rectangle> rectangles = new List<Rectangle>();
                // Loop until the Total Height is reached
                for (int i = 0; i < totalHeight; i += viewportHeight)
                {
                    int newHeight = viewportHeight;
                    // Fix if the Height of the Element is too big
                    if (i + viewportHeight > totalHeight)
                    {
                        newHeight = totalHeight - i;
                    }
                    // Loop until the Total Width is reached
                    for (int ii = 0; ii < totalWidth; ii += viewportWidth)
                    {
                        int newWidth = viewportWidth;
                        // Fix if the Width of the Element is too big
                        if (ii + viewportWidth > totalWidth)
                        {
                            newWidth = totalWidth - ii;
                        }
                        // Create and add the Rectangle
                        Rectangle currRect = new Rectangle(ii, i, newWidth, newHeight);
                        rectangles.Add(currRect);
                    }
                }

                // Build the Image
                stitchedImage = new Bitmap(totalWidth, totalHeight);
                // Get all Screenshots and stitch them together
                Rectangle previous = Rectangle.Empty;
                foreach (var rectangle in rectangles)
                {
                    // Calculate the Scrolling (if needed)
                    if (previous != Rectangle.Empty)
                    {
                        int xDiff = rectangle.Right - previous.Right;
                        int yDiff = rectangle.Bottom - previous.Bottom;
                        // Scroll
                        //selenium.RunScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        ((IJavaScriptExecutor)_driver).ExecuteScript(String.Format("window.scrollBy({0}, {1})", xDiff, yDiff));
                        System.Threading.Thread.Sleep(200);
                    }
                    // Take Screenshot
                    var screenshot = ((ITakesScreenshot)_driver).GetScreenshot();
                    // Build an Image out of the Screenshot
                    Image screenshotImage;
                    using (MemoryStream memStream = new MemoryStream(screenshot.AsByteArray))
                    {
                        screenshotImage = Image.FromStream(memStream);
                    }
                    // Calculate the Source Rectangle
                    Rectangle sourceRectangle = new Rectangle(viewportWidth - rectangle.Width, viewportHeight - rectangle.Height, rectangle.Width, rectangle.Height);
                    // Copy the Image
                    using (Graphics g = Graphics.FromImage(stitchedImage))
                    {
                        g.DrawImage(screenshotImage, rectangle, sourceRectangle, GraphicsUnit.Pixel);
                    }
                    // Set the Previous Rectangle
                    previous = rectangle;
                }
            }
            catch (Exception ex)
            {
                // handle
            }
            return stitchedImage;
        }

        public void TakeScreenshot()
        {
            var logsDir = ConfigurationManager.AppSettings["logsDir"] + "\\";
            logsDir = AppDomain.CurrentDomain.BaseDirectory + "\\" + logsDir;
            Bitmap memoryImage;
            //Set full width, height for image
            memoryImage = new Bitmap(Screen.PrimaryScreen.Bounds.Width,
                                   Screen.PrimaryScreen.Bounds.Height,
                                   PixelFormat.Format32bppArgb);
            Size s = new Size(memoryImage.Width, memoryImage.Height);
            Graphics memoryGraphics = Graphics.FromImage(memoryImage);
            memoryGraphics.CopyFromScreen(0, 0, 0, 0, s);
            string str = "";

            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
            string saveLocation = @logsDir;
            str = saveLocation + timestamp + ".png";
            memoryImage.Save(str);
            Console.WriteLine("Screenshot: {0}", new Uri(str));
        }

        public Image CaptureElementScreenShot(IWebElement element,IWebDriver driver)
        {
            var logsDir = ConfigurationManager.AppSettings["logsDir"] + "\\";
            logsDir = AppDomain.CurrentDomain.BaseDirectory + "\\" + logsDir;
            string saveLocation = @logsDir;
            string timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss-ffff");
            Screenshot screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            //screenshot.SaveAsFile(saveLocation + timestamp + "_ViewPort.Jpeg", ScreenshotImageFormat.Jpeg);
            MemoryStream stream = new MemoryStream(screenshot.AsByteArray);
            Image img = Bitmap.FromStream(stream);
            Rectangle rect = new Rectangle();

            if (element != null)
            {
                // Get the Width and Height of the WebElement using
                int width = (int)element.Size.Width * 75/100;
                int height = element.Size.Height * 75/100;
                // Get the Location of WebElement in a Point.
                // This will provide X & Y co-ordinates of the WebElement
                Point p = element.Location;
                // Create a rectangle using Width, Height and element location
                rect = new Rectangle(p.X * 75 / 100, p.Y * 75 / 100, width, height);
            }
            // croping the image based on rect.
            Bitmap bmpImage = new Bitmap(img);
            var cropedImag = bmpImage.Clone(rect, bmpImage.PixelFormat);
            bmpImage.Dispose();
           
            string str = "";
            timestamp = DateTime.Now.ToString("yyyy-MM-dd-hhmm-ss");
            saveLocation = @logsDir;
            str = saveLocation + timestamp + ".png";
            cropedImag.Save(str);

            return cropedImag;
        }

    }
}
