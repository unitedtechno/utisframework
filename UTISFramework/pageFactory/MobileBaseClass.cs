﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.MultiTouch;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UTISFramework.pageFactory
{
    public class MobileBaseClass
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        protected AndroidDriver<AppiumWebElement> Driver { get; set; }
        private static IWebDriver webdriver;

        public MobileBaseClass(AndroidDriver<AppiumWebElement> driver)
        {
            this.Driver = driver;
            PageFactory.InitElements(driver, this);
            // defaultTimeOutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["defaultTimeOutSeconds"]);
        }

        public MobileBaseClass(IWebDriver Driver)
        {
            webdriver = Driver;
        }

        public bool IsElementDisplayedAndEnabled(IWebElement element)
        {
            bool isEnabledAndDisplayed = false;
            try
            {
                isEnabledAndDisplayed = (element.Displayed && element.Enabled);
                return isEnabledAndDisplayed;
            }
            catch (Exception)
            {
                return isEnabledAndDisplayed;
            }
        }

        public bool WaitForElementToLoad(IWebElement element, int maxWaitTime = 180)
        {
            bool isElementLoaded = false;
            int waitTime = 0;
            while (waitTime < maxWaitTime)
            {
                if (IsElementDisplayedAndEnabled(element))
                {
                    isElementLoaded = true;
                    break;
                }
                Thread.Sleep(1000);
                waitTime++;
            }
            return isElementLoaded;
        }

        //Horizontal Swipe by percentages
        public void HorizontalSwipe(double startPercentage, double endPercentage, double anchorPercentage)
        {
            var size = Driver.Manage().Window.Size;
            int anchor = (int)(size.Height * anchorPercentage);
            int startPoint = (int)(size.Width * startPercentage);
            int endPoint = (int)(size.Width * endPercentage);
            TouchAction action = new TouchAction(Driver);
            action
                .Press(startPoint, anchor)
                .Wait(1000)
                .MoveTo(endPoint, anchor)
                .Release().Perform();

        }

        //Vertical Swipe by percentages
        public void VerticalSwipe(double startPercentage, double endPercentage, double anchorPercentage)
        {
            var size = Driver.Manage().Window.Size;          
            int anchor = (int)(size.Width * anchorPercentage);
            int startPoint = (int)(size.Height * startPercentage);
            int endPoint = (int)(size.Height * endPercentage);
            TouchAction action = new TouchAction(Driver);
            action
                .Press(anchor, startPoint)
                .Wait(2000)
                .MoveTo(anchor, endPoint)
                .Release().Perform();

        }

        //Vertical Swipe by percentages
        public void PressByCoordinates(double startPercentage, double endPercentage)
        {
            var size = Driver.Manage().Window.Size;
            int startPoint = (int)(size.Width * startPercentage);
            int endPoint = (int)(size.Height * endPercentage);
            TouchAction action = new TouchAction(Driver);
            action
                .Press(startPoint, endPoint)
                .Wait(2000)
                .Release().Perform();

        }

        public void DoubleTapByCoordinates(double startPercentage, double endPercentage)
        {
            var size = Driver.Manage().Window.Size;
            int startPoint = (int)(size.Width * startPercentage);
            int endPoint = (int)(size.Height * endPercentage);
            TouchAction action = new TouchAction(Driver);
            action
                .Tap(startPoint, endPoint,2)
                .Wait(2000)
                .Perform();

        }
    }
}
