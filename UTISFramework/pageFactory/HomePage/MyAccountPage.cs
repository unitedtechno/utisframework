﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UTISFramework.pageFactory.HomePage
{
    public class MyAccountPage : BaseClass
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public MyAccountPage(IWebDriver driver) : base(driver)
        {

        }

        [FindsBy(How = How.XPath, Using = "//span[@title='My Orders']")]
        public IWebElement MyOrder { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_first_name")]
        public IWebElement FirstName { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_last_name")]
        public IWebElement LastName { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_company")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_address")]
        public IWebElement StreetAddress1 { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_address2")]
        public IWebElement StreetAddress2 { get; set; }

        [FindsBy(How = How.Id, Using = "Country")]
        public IWebElement Country { get; set; }

        [FindsBy(How = How.Id, Using = "x_ship_to_city")]
        public IWebElement City { get; set; }

        [FindsBy(How = How.Id, Using = "UsStates")]
        public IWebElement State { get; set; }

        [FindsBy(How = How.Id, Using = "Zipcode")]
        public IWebElement ZipCode { get; set; }

        [FindsBy(How = How.Id, Using = "Phone")]
        public IWebElement Phone { get; set; }

        [FindsBy(How = How.LinkText, Using = "Continue")]
        public IWebElement Continue { get; set; }

    }
}
