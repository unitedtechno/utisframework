﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UTISFramework.pageFactory.HomePage
{
    public class LoginPage :BaseClass
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public LoginPage(IWebDriver driver) : base(driver)
        {

        }

        [FindsBy(How = How.XPath, Using = "//input[@title='Email Address']")]
        public IWebElement UserName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@title='Password']")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@title='Sign In']")]
        public IWebElement SignInButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//p[@class='message']")]
        public IWebElement LoginErrorMessage { get; set; }


        //Create an Account

        [FindsBy(How = How.XPath, Using = "//input[@title='Email Address*']")]
        public IWebElement NewUserName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@title='Confirm Email Address*']")]
        public IWebElement ConfirmUserName { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@title='Password*']")]
        public IWebElement NewPassword { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@title='Confirm Password*']")]
        public IWebElement ConfirmPassword { get; set; }

        [FindsBy(How = How.Id, Using = "cust_type")]
        public IWebElement CustomerBusinessType { get; set; }

        [FindsBy(How = How.Id, Using = "vertical_market")]
        public IWebElement VerticalMarket { get; set; }

        [FindsBy(How = How.Id, Using = "company_name")]
        public IWebElement CompanyName { get; set; }

        [FindsBy(How = How.Id, Using = "no_employees")]
        public IWebElement NoOfEmployees { get; set; }

        [FindsBy(How = How.Id, Using = "industry")]
        public IWebElement Industry { get; set; }

        [FindsBy(How = How.Id, Using = "company_website")]
        public IWebElement CompanyWebsite { get; set; }

        [FindsBy(How = How.Id, Using = "employer_id_number")]
        public IWebElement EIN { get; set; }

        [FindsBy(How = How.Id, Using = "x_first_name")]
        public IWebElement FirstName { get; set; }

        [FindsBy(How = How.Id, Using = "x_last_name")]
        public IWebElement LastName { get; set; }

        [FindsBy(How = How.Id, Using = "x_address")]
        public IWebElement Address { get; set; }

        [FindsBy(How = How.Id, Using = "x_address2")]
        public IWebElement Address2 { get; set; }

        [FindsBy(How = How.Id, Using = "Country")]
        public IWebElement Country { get; set; }

        [FindsBy(How = How.Id, Using = "x_city")]
        public IWebElement City { get; set; }

        [FindsBy(How = How.Id, Using = "UsStates")]
        public IWebElement State { get; set; }

        [FindsBy(How = How.Id, Using = "x_zip")]
        public IWebElement ZipCode { get; set; }

        [FindsBy(How = How.Id, Using = "x_phone")]
        public IWebElement Phone { get; set; }

        [FindsBy(How = How.XPath, Using = "//input[@value='Register']")]
        public IWebElement RegisterButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//i[@class='fa fa-search mobile-search-icon']")]
        public IWebElement ProductSearchIcon { get; set; }

        [FindsBy(How = How.Id, Using = "keyword")]
        public IWebElement ProductSearch { get; set; }

        [FindsBy(How = How.Id, Using = "add-to-cart-qty")]
        public IWebElement Quantity { get; set; }

        [FindsBy(How = How.Id, Using = "addtocartqty")]
        public IWebElement AddToCart { get; set; }

        [FindsBy(How = How.XPath, Using = "(//a[@title='Proceed to Checkout'])[1]")]
        public IWebElement ProceedToCheckout { get; set; }

        [FindsBy(How = How.LinkText, Using = "Mail Payment")]
        public IWebElement MailPayment { get; set; }

        [FindsBy(How = How.LinkText, Using = "Continue")]
        public IWebElement Continue { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[text()='Place Order']")]
        public IWebElement PlaceOrder { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[contains(text(),'Order number:')]")]
        public IWebElement OrderNumber { get; set; }

        internal void Login(string username, string password)
        {
            SetUserName(username);
            SetPassword(password);        
            SignInButton.Click();

        }

        internal void CreateAnAccount(string username, string password)
        {
            Type(NewUserName, username);
            Type(ConfirmUserName, username);
            Console.WriteLine($"Username Entered: {username}");
            Type(NewPassword, password);
            Type(ConfirmPassword, password);
            Console.WriteLine($"Password Entered: ******** ");
        }

        private void SetPassword(string password)
        {
            WaitForElementToLoad(Password);
            Password.Clear();
            Password.SendKeys(password);
        }

        private void SetUserName(string username)
        {
            WaitForElementToLoad(UserName);
            UserName.Clear();
            UserName.SendKeys(username);
        }
    }
}
