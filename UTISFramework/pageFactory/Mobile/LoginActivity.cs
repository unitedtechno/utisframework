﻿using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UTISFramework.pageFactory.Mobile
{
    public class LoginActivity :MobileBaseClass
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public LoginActivity(AndroidDriver<AppiumWebElement> driver) : base(driver)
        {


        }


        //SIGN IN Button
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[0]")]
        public IWebElement SignInButton { get; set; }

        //SIGN UP Button
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[1]")]
        public IWebElement SignUPButton { get; set; }

        //Enter Now Button
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[2]")]
        public IWebElement EnterNowButton { get; set; }

        //Enter Now Button
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[@text='SHOP']")]
        public IWebElement ShopButton { get; set; }

        //Clothing Option
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[@text='CLOTHING']")]
        public IWebElement ClothingMenu { get; set; }

        //MEN Option
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[@text='MEN']")]
        public IWebElement MenOption { get; set; }

        //First Product Option       
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[@text='Skull Sunrise Hawaiian Shirt']")]
        public IWebElement FirstProductInList { get; set; }

        //AddToCart Button       
        [FindsBy(How = How.XPath, Using = "//android.widget.TextView[@text='ADD TO CART']")]
        public IWebElement FlipkartAddToCart { get; set; }

        //AddToCart Button       
        [FindsBy(How = How.XPath, Using = "//android.widget.Button[@text='ADD TO CART']")]
        public IWebElement AddToCart { get; set; }

        //CartIcon       
        [FindsBy(How = How.Id, Using = "com.blacksunpro.metallica:id/action_cart_image")]
        public IWebElement CartIcon { get; set; }

        //CheckOut Button       
        [FindsBy(How = How.XPath, Using = "//android.widget.Button[@text='CHECKOUT']")]
        public IWebElement CheckOutButton { get; set; }

        //Continue As Guest Button      
        [FindsBy(How = How.XPath, Using = "//android.widget.Button[@text='Continue as Guest']")]
        public IWebElement ContinueAsGuestButton { get; set; }

        //Continue As Guest Button      
        [FindsBy(How = How.XPath, Using = "//android.widget.Button[@text='PLACE ORDER']")]
        public IWebElement PlaceOrderButton { get; set; }

        //Continue As Guest Button      
        [FindsBy(How = How.Id, Using = "android:id/search_mag_icon")]
        public IWebElement SearchIcon { get; set; }

        //Continue As Guest Button      
        [FindsBy(How = How.Id, Using = "android:id/search_src_text")]
        public IWebElement SearchField { get; set; }

        




    }
}
