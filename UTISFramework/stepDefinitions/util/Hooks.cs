﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using TechTalk.SpecFlow;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using log4net.Config;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System.Security.Policy;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Threading;
using System.Collections.Specialized;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Net.NetworkInformation;
using System.Net;
using OpenQA.Selenium.Safari;
using System.Runtime.CompilerServices;
using BoDi;
using System.Collections.ObjectModel;
using System.Collections;
using UTISFramework.pageFactory.HomePage;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Service;

namespace UTISFramework.stepDefinitions.Hooks
{
    [Binding]
    public class Hooks
    {
        private static readonly ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string featureTitle;
        private readonly string[] featureTags;
        private readonly string featureDescription;
        private readonly string[] scenarioTags;
        private readonly string scenarioTitle;
        private int defaultTimeOutSeconds;
        private static Stopwatch stepduration;
        private readonly IObjectContainer _objectContainer;
        public IWebDriver driver;
        private static AndroidDriver<AppiumWebElement> _driver;
        private static AppiumLocalService _appiumLocalService;
        private static Uri testServerAddress = new Uri("http://127.0.0.1:4723/wd/hub");
        public ICapabilities capabilities;

        MyAccountPage objMyAccount;

        public Hooks(IObjectContainer objectContainer)
        {
            XmlConfigurator.Configure();
            defaultTimeOutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["defaultTimeOutSeconds"]);
            //hubTimeOut = defaultTimeOutSeconds + 10;
            featureTags = FeatureContext.Current.FeatureInfo.Tags;
            featureDescription = FeatureContext.Current.FeatureInfo.Description;
            scenarioTitle = ScenarioContext.Current.ScenarioInfo.Title;
            scenarioTags = ScenarioContext.Current.ScenarioInfo.Tags;
            _objectContainer = objectContainer;

        }
        [BeforeFeature()]
        public static void BeforeFeatureBegins()
        {
            featureTitle = FeatureContext.Current.FeatureInfo.Title;
            logger.Debug("***** Begin Feature: " + featureTitle + " *****");
            Console.WriteLine($"[Begin Feature] - {featureTitle}");

            var _browser = ConfigurationManager.AppSettings["defaultBrowser"].ToLower();
            string _env = ConfigurationManager.AppSettings["env"].ToLower();
            logger.Debug("***** Testing Environment: " + _env + " *****");
            Console.WriteLine($"Testing Environment: {_env}");
            logger.Debug("***** Browser: " + _browser + " *****");
            Console.WriteLine($"Browser: {_browser}");

        }
        [BeforeScenario]
        public void BeforeScenario()
        {
            var browser = ConfigurationManager.AppSettings["defaultBrowser"].ToLower();
        //    var mode = ConfigurationManager.AppSettings["seleniumMode"].ToLower();

            Console.WriteLine("[BeforeScenario] - Start " + browser);
            logger.Debug("***** Begin Scenario: " + scenarioTitle + " **");
            Console.WriteLine("Scenario: " + scenarioTitle);

            switch (browser)
            {
                case "chrome":
                    StartChrome();
                    break;
                case "firefox":
                    StartFF();
                    break;
                case "ie":
                    StartIE();
                    break;
                case "safari":
                   //TODO Safari
                    break;
                case "androidapp":
                    StartAndroidApp();
                    break;
                case "androidweb":
                    StartAndroidChrome();
                    break;
                case "browserstack":
                    StartBrowerStack();
                    break;
                case "chromeipad":
                    StartChromeIpad();
                    break;
                default:
                    Console.WriteLine($"Invalid Browser Name Configured: {browser}");
                    StartChrome();
                    break;
            }



        }

        [BeforeStep]
        public void BeforeStepBegins()
        {
            logger.Debug("***** " + ScenarioContext.Current.StepContext.StepInfo.StepDefinitionType + ": " + ScenarioContext.Current.StepContext.StepInfo.Text);
            stepduration = Stopwatch.StartNew();
          
        }

        [AfterStep]
        public void AfterStepEnds()
        {
            stepduration.Stop();
            logger.Debug($"Step Execution Time : {stepduration.Elapsed}");
            
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var browser = ConfigurationManager.AppSettings["defaultBrowser"].ToLower();
            logger.Debug("***** End Scenario: " + scenarioTitle + " **");
            Console.WriteLine("[AfterScenario] - Quit browser");
            bool testPass = true;

            if (ScenarioContext.Current.TestError != null)
            {
                objMyAccount = new MyAccountPage(driver);
                string errMsg = "error: " + ScenarioContext.Current.TestError.Message;
                logger.Debug(errMsg);
                Console.WriteLine(errMsg);
                //driver.Manage().Cookies.AddCookie(cookieTestFail);
                testPass = false;
                try
                {
                    objMyAccount.TakeScreenshot(driver);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Screenshot error: " + ex);
                    objMyAccount.TakeScreenshot();
                    
                }
            }

            if (browser.Equals("chrome"))
            {
                closeDriver();
                KillProcByName("chromedriver");
            }
            else if (browser.Equals("firefox"))
            {
                closeDriver();
                KillProcByName("firefox");
            }
            else if (browser.Equals("ie"))
            {
                closeDriver();
                KillProcByName("internetexplorer");
            }
            else if (browser.Equals("browserstack"))
            {
                closeDriver();
            }
            else if (browser.Equals("androidapp"))
            {

               
                

            }





        }

        [AfterFeature]
        public static void AfterFeature()
        {
            logger.Debug("***** End Feature: " + featureTitle + " *****");
            Console.WriteLine("[End Feature] - " + featureTitle);
        }

        private void StartChrome(Boolean localHub = false)
        {         
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--no-sandbox");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--start-maximized");
            options.AddArgument("--test-type");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--disable-infobars");
            options.AddArguments("chrome.switches", "--disable-extensions");
            options.AddUserProfilePreference("credentials_enable_service", false);
            options.AddUserProfilePreference("profile.password_manager_enabled", false);


            //var browserDriverConfig = (NameValueCollection)ConfigurationManager.GetSection("browserDriverConfig");
            //var chromeDriverDirectory = browserDriverConfig["chromeDriverDirectory"].ToLower();
            //string exactPath = System.IO.Path.GetFullPath(chromeDriverDirectory);
            defaultTimeOutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["defaultTimeOutSeconds"]);
            driver = new ChromeDriver(options);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var agent = js.ExecuteScript("return navigator.userAgent");
            Console.WriteLine("userAgent = " + agent);
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(defaultTimeOutSeconds));
            writeBrowserInfoToConsole(driver);
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();          
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
        }
        private void StartChromeIpad(Boolean localHub = false)
        {
            ChromeOptions options = new ChromeOptions();
            options.EnableMobileEmulation("iPad");
            options.AddArgument("--no-sandbox");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--start-maximized");
            options.AddArgument("--test-type");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--disable-infobars");
            options.AddArguments("chrome.switches", "--disable-extensions");
            options.AddUserProfilePreference("credentials_enable_service", false);
            options.AddUserProfilePreference("profile.password_manager_enabled", false);


            //var browserDriverConfig = (NameValueCollection)ConfigurationManager.GetSection("browserDriverConfig");
            //var chromeDriverDirectory = browserDriverConfig["chromeDriverDirectory"].ToLower();
            //string exactPath = System.IO.Path.GetFullPath(chromeDriverDirectory);
            defaultTimeOutSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["defaultTimeOutSeconds"]);
            driver = new ChromeDriver(options);
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            var agent = js.ExecuteScript("return navigator.userAgent");
            Console.WriteLine("userAgent = " + agent);
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(defaultTimeOutSeconds));
            writeBrowserInfoToConsole(driver);
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
        }


        private void StartFF()
        {
           
            FirefoxDriverService service = FirefoxDriverService.CreateDefaultService();
           // service.FirefoxBinaryPath = @ffExe;
            service.HideCommandPromptWindow = true;
            service.SuppressInitialDiagnosticInformation = true;
            FirefoxOptions options = new FirefoxOptions();
            options.AddArgument("--no-sandbox");
            options.AddArgument("--ignore-certificate-errors");
            options.AddArgument("--start-maximized");
            options.AddArgument("--test-type");
            options.AddArgument("--disable-gpu");
            options.AddArgument("--disable-infobars");           
            driver = new FirefoxDriver(service, options, TimeSpan.FromSeconds(30));
            capabilities = ((RemoteWebDriver)driver).Capabilities;
            //WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            driver.Manage().Window.Maximize();
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
            writeBrowserInfoToConsole(driver);
        }

        private void StartIE()
        {
            var options = new InternetExplorerOptions();
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.EnsureCleanSession = true;            
            driver = new InternetExplorerDriver(options);
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromMinutes(1));
            capabilities = ((RemoteWebDriver)driver).Capabilities;
            driver.Manage().Window.Maximize();
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
            writeBrowserInfoToConsole(driver);
        }
        private void StartAndroidApp()
        {
            _appiumLocalService = new AppiumServiceBuilder().UsingAnyFreePort().Build();
            _appiumLocalService.Start();
            var appiumOptions = new AppiumOptions();
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.DeviceName, "PL2GAR4852919944");
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            appiumOptions.AddAdditionalCapability(MobileCapabilityType.PlatformVersion, "10");
            //appiumOptions.AddAdditionalCapability(MobileCapabilityType.App, "E:\\Jayakumar\\APK\\app-debug.apk");
            _driver = new AndroidDriver<AppiumWebElement>(testServerAddress, appiumOptions);


            // _driver?.LaunchApp();
            // _driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            // _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            _objectContainer.RegisterInstanceAs<AndroidDriver<AppiumWebElement>>(_driver);
          //writeBrowserInfoToConsole(driver);
        }

        private void StartBrowerStack()
        {
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserName", "android");
            capability.SetCapability("device", "Samsung Galaxy S8");
            capability.SetCapability("realMobile", "true");
            capability.SetCapability("os_version", "7.0");
            capability.SetCapability("browserstack.user", "jayakumarthangam1");
            capability.SetCapability("browserstack.key", "y1Z5AHRXGXJhB494wMEE");
            capability.SetCapability("name", "MonoPrice Web");
            capability.SetCapability("project", "BrowserStak Demo");

            driver = new RemoteWebDriver(
              new Uri("http://hub-cloud.browserstack.com/wd/hub/"), capability
            );           
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
            writeBrowserInfoToConsole(driver);
        }

        private void StartAndroidChrome()
        {
            DesiredCapabilities capability = new DesiredCapabilities();
            capability.SetCapability("browserName", "chrome");
            capability.SetCapability("device", "PL2GAR4852919944");
            capability.SetCapability("platformName", "android");
            capability.SetCapability("platformVersion", "10");


            driver = new RemoteWebDriver(
              testServerAddress, capability
            );
            ScenarioContext.Current["mainWindowHandle"] = driver.WindowHandles.FirstOrDefault();
            _objectContainer.RegisterInstanceAs<IWebDriver>(driver);
            writeBrowserInfoToConsole(driver);
        }


        public void KillProcByName(string processName)
        {
            Process[] localAll = Process.GetProcesses();
            try
            {
                foreach (Process proc in Process.GetProcessesByName(processName))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                logger.Debug(ex.Message);
            }
        }

        public void closeDriver()
        {
            try
            {
                driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(0);
                //driver.Manage().Cookies.AddCookie(new OpenQA.Selenium.Cookie("zaleniumTestPassed", testPass.ToString()));
            }
            catch (NullReferenceException npe)
            {
                Console.WriteLine("NullReferenceException: The HTTP request to the remote WebDriver server may have timedout during initialization.", Color.Red);
                Console.WriteLine(npe.ToString(), Color.Red);
            }
            catch (WebDriverException wde)
            {

                Console.WriteLine("WebDriverException: The HTTP request to the remote WebDriver server for URL timed out after defaultTimeSeconds", Color.Red);
                Console.WriteLine(wde.ToString(), Color.Red);
            }
            finally
            {
                driver.Dispose();
                driver.Quit();
            }
        }

        public void writeBrowserInfoToConsole(IWebDriver driver)
        {
            capabilities = ((RemoteWebDriver)driver).Capabilities;
            var browserName = capabilities.GetCapability("browserName");
            var browserVersion = capabilities.GetCapability("browserVersion");
            Console.WriteLine("Browser Name: " + browserName);
            Console.WriteLine("Browser Version: " + browserVersion);
        }

    }
}
