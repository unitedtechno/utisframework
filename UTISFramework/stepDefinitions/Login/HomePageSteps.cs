﻿using UTISFramework.pageFactory.HomePage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace UTISFramework.stepDefinitions.Login
{
    [Binding]
    public class HomePageSteps
    {
        private readonly string baseUrl = ScenarioContext.Current["baseUrl"].ToString();


        IWebDriver driver;
        LoginPage objLogin;
        MyAccountPage objMyAccount;
        HomePage objHomePage;
        
        public HomePageSteps(IWebDriver Driver)
        {
            driver = Driver;
            objLogin = new LoginPage(driver);
            objMyAccount = new MyAccountPage(driver);
            objHomePage = new HomePage(driver);

        }

        [Then(@"I Open any Product Page")]
        public void ThenIOpenAnyProductPage()
        {
            objHomePage.ClickByJS(objHomePage.CompanyLogo);
            objHomePage.ClickByJS(objHomePage.ShopMonoPriceButton);
            objHomePage.ClickByJS(objHomePage.Cables);
            objHomePage.ClickByJS(objHomePage.HdmiCables);
            objHomePage.ClickByJS(objHomePage.AddToCart_9303);
            objHomePage.WaitForElementToLoad(objHomePage.Checkout,20);
            objHomePage.ClickByJS(objHomePage.Checkout);
            objHomePage.ClickByJS(objHomePage.ProceedToCheckout);
            objHomePage.CVV.SendKeys("786");
            objHomePage.ClickByJS(objHomePage.PlaceOrder);
            var orderid=objHomePage.OrderNumber.Text;
            Console.WriteLine(orderid);

        }


    }
}
