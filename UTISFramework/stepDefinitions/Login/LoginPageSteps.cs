﻿using UTISFramework.pageFactory.HomePage;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using System.IO;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace UTISFramework.stepDefinitions.Login
{
    [Binding]
    public sealed class LoginPageSteps
    {
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef
        private readonly string baseUrl = ScenarioContext.Current["baseUrl"].ToString();


        IWebDriver driver;
        LoginPage objLogin;
        MyAccountPage objMyAccount;

        public LoginPageSteps(IWebDriver Driver)
        {
            driver = Driver;
            objLogin = new LoginPage(driver);
            objMyAccount = new MyAccountPage(driver);

        }
        [Then(@"I Navigate to MyAccount Login Page")]
        public void ThenINavigateToMyAccountPage()
        {

            driver.Navigate().GoToUrl($"{baseUrl}/myaccount/myaccount");
            Console.WriteLine("Navigated to Login Page Successfully");
        }



        [When(@"I log in using Username as ""(.*)"" and Password ""(.*)"" on the login page")]
        public void WhenILogInUsingUsernameAsAndPasswordOnTheLoginPage(string username, string password)
        {
            objLogin.Login(username,password);
            Console.WriteLine($"Logged in Successfully as User : {username}");
        }

        [Then(@"I should be logged in")]
        public void ThenIShouldBeLoggedIn()
        {
            Thread.Sleep(2000);
            Assert.True(objMyAccount.WaitForElementToLoad(objMyAccount.MyOrder,30));

        }

        [Then(@"I should not be logged in")]
        public void ThenIShouldNotBeLoggedIn()
        {
            objLogin.WaitForElementToLoad(objLogin.LoginErrorMessage);
            Console.WriteLine(objLogin.LoginErrorMessage.Text,Color.Red);
            Assert.True(objLogin.IsElementDisplayedAndEnabled(objLogin.LoginErrorMessage));
               
        }

        [Given(@"I Open a Website")]
        public void GivenIOpenAWebsite()
        {
            driver.Navigate().GoToUrl("https://www.facebook.com/");
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("document.body.style.zoom='75%'");
            Thread.Sleep(2000);
            var element=driver.FindElement(By.XPath("(//div[@class='lfloat _ohe'])[2]"));
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            var cropedImage= objLogin.CaptureElementScreenShot(element,driver);
            
        }

        [Given(@"Launch the MonoPrice Web App")]
        public void GivenLaunchTheMonoPriceWebApp()
        {
            // driver.StartActivity("com.android.chrome", "org.chromium.chrome.browser.ChromeTabbedActivity");

        }

        [Given(@"I Create an account using Username as ""(.*)"" and Password ""(.*)""")]
        [Then(@"I Create an account using Username as ""(.*)"" and Password ""(.*)""")]
        public void ThenICreateAnAccountUsingUsernameAsAndPassword(string username, string password)
        {
            //Random randomGenerator = new Random();
            //int randomInt = randomGenerator.Next(1000);
            objLogin.WaitForPageLoading();
            username = $"jayakumart+AutomationUser{DateTime.Now:yyyyMMddhhmmssfff}@unitedtechno.com";
            password = "Welcome@88";
            objLogin.CreateAnAccount(username, password);
        }

        [Then(@"I Click Register Button")]
        public void ThenIClickRegisterButton()
        {
            objLogin.Click(objLogin.RegisterButton);
        }

        [Then(@"I Open the Product Page for the product ID '(.*)'")]
        public void ThenIOpenTheProductPageForTheProductID(string value)
        {
            driver.Navigate().GoToUrl($"{baseUrl}/product?p_id=3992");

        }

        [Then(@"I change the product quantity to '(.*)' and click AddToCart Button")]
        public void ThenIChangeTheProductQuantityToAndClickAddToCartButton(int quantity)
        {
            objLogin.Type(objLogin.Quantity, quantity.ToString());
            objLogin.VerifyText(objLogin.Quantity, quantity.ToString());
            objLogin.Click(objLogin.AddToCart);
            objLogin.WaitForPageLoading();
        }

        [Then(@"I Navigate to Shopping Cart Page")]
        public void ThenINavigateToShoppingCartPage()
        {
            objLogin.OpenURL($"{baseUrl}/cart/index");
        }

        [Then(@"I Click Proceed To Check Out")]
        public void ThenIClickProceedToCheckOut()
        {
            objLogin.ClickByJS(objLogin.ProceedToCheckout);
        }

        [Then(@"I fill all the shipping details")]
        public void ThenIFillAllTheShippingDetails()
        {
            objLogin.Type(objMyAccount.FirstName, "AutomationUser");
            objLogin.Type(objMyAccount.LastName, "Test");
            objLogin.Type(objMyAccount.StreetAddress1, "13401 Blackbird St");
            objLogin.Type(objMyAccount.City, "Garden Grove");
            objLogin.SelectDropdownByVisibleText(objMyAccount.State, "CALIFORNIA");
            objLogin.Type(objMyAccount.ZipCode, "92843");
            objLogin.Type(objMyAccount.Phone, "9841058680");
            Thread.Sleep(2000);
            objLogin.Click(objMyAccount.Continue);
        }

        [Then(@"Proceed with ""(.*)"" as ""(.*)"" to place an order")]
        public void ThenProceedWithToPlaceAnOrder(string paymentmethod, string usertype)
        {
            string walletcredentials, username, password;
            JObject walletjObj;
            
            switch (paymentmethod)
            {
                case "MailPayment":
                    objLogin.Click(objLogin.MailPayment);
                    objLogin.Click(objLogin.Continue);
                    break;
                
                default:
                    Console.WriteLine($"UnKnown Payment method provided. {paymentmethod}");
                    throw new Exception($"UnKnown Payment method provided. {paymentmethod}");
            }
            


        }

        [Then(@"I Click Place an Order")]
        public void ThenIClickPlaceAnOrder()
        {
            objLogin.ClickByJS(objLogin.PlaceOrder);
            var orderid = objLogin.OrderNumber.Text;
            Console.WriteLine(orderid);
        }



    }
}
