﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechTalk.SpecFlow;
using UTISFramework.pageFactory;
using UTISFramework.pageFactory.Mobile;

namespace UTISFramework.stepDefinitions.Mobile
{
    [Binding]
    public sealed class LoginActivitySteps
    {
        private static AndroidDriver<AppiumWebElement> driver;
        private static IWebDriver webdriver;
        public LoginActivity objLoginActivity;
        public LoginActivitySteps(AndroidDriver<AppiumWebElement> Driver)
        {
            driver = Driver;
            objLoginActivity = new LoginActivity(Driver);
        }

        [Given(@"I Click the Dashboard")]
        public void GivenIClickTheDashboard()
        {
            driver.StartActivity("com.flipkart.android", "SplashActivity");
           // driver.FindElementByXPath("//*[@text='Dashboard']").Click();
        }

        [Given(@"Launch the Flipkart App")]
        public void GivenLaunchTheFlipkartApp()
        {
            driver.StartActivity("com.flipkart.android", "com.flipkart.android.activity.HomeFragmentHolderActivity");
        }

        [Given(@"Launch the Metallica App")]
        public void GivenLaunchTheMetallicaApp()
        {
            driver.StartActivity("com.blacksunpro.metallica", "com.predictspring.mobile.consumer.ui.waitformobileconfig.WaitForMobileConfigActivity");
            Console.WriteLine($"Launching Metallica App Using Start Activity");
        }

        [Then(@"Click Enter Now Button")]
        public void ThenClickEnterNowButton()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.EnterNowButton,10);
            objLoginActivity.EnterNowButton.Click();
            Console.WriteLine($"Clicked Enter Now Button Successfully");
        }

        [Then(@"CLick Shop Button")]
        public void ThenCLickShopButton()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.ShopButton,10);
            objLoginActivity.ShopButton.Click();
            Console.WriteLine($"Clicked Shop Button Successfully");
        }

        [Then(@"Click Clothing Menu")]
        public void ThenClickClothingMenu()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.ClothingMenu, 10);
            objLoginActivity.ClothingMenu.Click();
            Console.WriteLine($"Clicked Clothing Menu Button Successfully");
        }

        [Then(@"Click '(.*)' Option")]
        public void ThenClickOption(string value)
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.MenOption, 10);
            objLoginActivity.MenOption.Click();
            Console.WriteLine($"Clicked MEN Option Successfully");
        }

        [Then(@"Select the Product from the List")]
        public void ThenSelectTheProductFromTheList()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.FirstProductInList, 10);
            objLoginActivity.VerticalSwipe(0.8,0.2,0.5);
            objLoginActivity.VerticalSwipe(0.8,0.2,0.5);
            Console.WriteLine($"Performed Vertical Swipe Successfully");
            //Console.WriteLine(driver.PageSource);
            objLoginActivity.SearchIcon.Click();
            Console.WriteLine($"Typed Search Keyword successfully");
            objLoginActivity.WaitForElementToLoad(objLoginActivity.SearchField, 10);
            objLoginActivity.SearchField.Clear();
            objLoginActivity.SearchField.SendKeys("Skull Sunrise");
            driver.HideKeyboard();
            objLoginActivity.FirstProductInList.Click();
            Console.WriteLine($"Opening the First Product");
        }

        [Then(@"Open the Product Image")]
        public void ThenOpenTheProductImage()
        {
            objLoginActivity.HorizontalSwipe(0.8, 0.2, 0.5);
            Console.WriteLine($"Performed Horizontal Swipe Successfully");
            objLoginActivity.PressByCoordinates(0.5,0.5);
            Thread.Sleep(2000);
            Console.WriteLine($"Performed Zoom In Successfully");
            objLoginActivity.DoubleTapByCoordinates(0.5, 0.5);
            Thread.Sleep(2000);
            objLoginActivity.DoubleTapByCoordinates(0.5, 0.5);
            Thread.Sleep(2000);
            objLoginActivity.DoubleTapByCoordinates(0.5, 0.5);
            Console.WriteLine($"Performed Zoom Out Successfully");
        }

        [Then(@"Go Back to Cart Page")]
        public void ThenGoBackToCartPage()
        {
            driver.Navigate().Back();
            Console.WriteLine($"Back the Add to Cart Page");
        }


        [Then(@"Click Add To Cart Button")]
        public void ThenClickAddToCartButton()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.AddToCart, 10);           
            objLoginActivity.AddToCart.Click();
            Console.WriteLine($"Clicked Add to Cart Button");
        }

        [Then(@"Click Add To Cart Button for flipkart")]
        public void ThenClickAddToCartButtonForFlipkart()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.FlipkartAddToCart, 10);
            objLoginActivity.FlipkartAddToCart.Click();
        }


        [Then(@"Click Cart Icon")]
        public void ThenClickCartIcon()
        {
            Thread.Sleep(5000);
            objLoginActivity.WaitForElementToLoad(objLoginActivity.CartIcon, 10);
            objLoginActivity.CartIcon.Click();
            Console.WriteLine($"Clicked Cart Icon");
        }

        [Then(@"Click CheckOut")]
        public void ThenClickCheckOut()
        {
            Thread.Sleep(5000);
            objLoginActivity.WaitForElementToLoad(objLoginActivity.CheckOutButton, 10);
            objLoginActivity.CheckOutButton.Click();
            Console.WriteLine($"Clicked Checkout Button");

        }

        [Then(@"Click Continue As Guest")]
        public void ThenClickContinueAsGuest()
        {
            objLoginActivity.WaitForElementToLoad(objLoginActivity.ContinueAsGuestButton, 10);
            objLoginActivity.ContinueAsGuestButton.Click();
            Console.WriteLine($"Clicked Continue As Guest Button");
        }

        [Then(@"Verify the User Navigated to Place Order Screen")]
        public void ThenVerifyTheUserNavigatedToPlaceOrderScreen()
        {
            Console.WriteLine($"Navigated to Place Order Screen");
            Assert.IsTrue(objLoginActivity.WaitForElementToLoad(objLoginActivity.PlaceOrderButton, 10));
           // Console.WriteLine(driver.PageSource);
            
        }

        




        [Then(@"I Search for a Product ""(.*)""")]
        public void ThenISearchForAProduct(string value)
        {
            driver.FindElementByXPath("//*[@text=' Search for Products, Brands and More']").Click();
            driver.FindElementByXPath("//*[@text='Search for Products, Brands and More']").SendKeys(value);
            driver.FindElementByXPath("//*[@text='laptop']").Click();
            Thread.Sleep(5000);          
            
        }

        [Then(@"Click First Product From the List")]
        public void ThenClickFirstProductFromTheList()
        {
            driver.FindElementByXPath("//*[@text='Lenovo IdeaPad Gaming 3i Core i5 10th Gen - (8 GB/1 TB HDD/256 GB SSD/Windows 10 Home/4 GB Graphics/NVIDIA Geforce GTX 1650 Ti) 15IMH05 Gaming Laptop']").Click();
            Thread.Sleep(2000);
        }

        [Then(@"Click Skip and GoToCart Button")]
        public void ThenClickSkipAndGoToCartButton()
        {
            driver.FindElementByXPath("//*[@text='SKIP & GO TO CART']").Click();
            Thread.Sleep(2000);
        }

        [Then(@"Verify the App navigated to Cart Page")]
        public void ThenVerifyTheAppNavigatedToCartPage()
        {
            var element= driver.FindElementByXPath("//*[@text='Place Order ']");
            Assert.IsTrue(element.Displayed);
            //driver.CloseApp();
            //driver.Lock();

        }


    }
}
