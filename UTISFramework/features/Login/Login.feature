﻿Feature: Login: Login Functionality Feature
    In order to place the orders
    As a user of the website
    I want to log into the website

@web @mobile @flipkart
Scenario: Flipkart Add a product to the Cart
	Given Launch the Flipkart App
	Then I Search for a Product "Laptop"
	Then Click First Product From the List
	Then Click Add To Cart Button for flipkart
	Then Click Skip and GoToCart Button
	Then Verify the App navigated to Cart Page

@web
Scenario Outline: UnSuccessful Login with InValid Credentials
	Given I open the "<site>" website's login page
	Then I Navigate to MyAccount Login Page
	When I log in using Username as "<UserName>" and Password "<Password>" on the login page
	Then I should not be logged in
Examples: 
| Description      | site          | UserName                  | Password |
| Invalid Username | Monoprice Web | jayakuma@unitedtechno.com | Welcome  |

@web
Scenario Outline: Place an Order
	Given I open the "<site>" website's login page
	Then I Navigate to MyAccount Login Page
	When I log in using Username as "<UserName>" and Password "<Password>" on the login page
	Then I Open any Product Page
Examples: 
| Description   | site          | UserName                    | Password   |
| Online Portal | Monoprice Web | jayakumart@unitedtechno.com | Welcome@88 |


@web
Scenario: Screen Shot Crop
	Given I open the "Monoprice Web" website's login page
	Given I Open a Website



@web @mobile @metallica @demo
Scenario: Guest CheckOut in Metallica Mobile App
	Given Launch the Metallica App
	Then Click Enter Now Button
	Then CLick Shop Button
	Then Click Clothing Menu
	Then Click 'MEN' Option
	Then Select the Product from the List
	Then Open the Product Image
	Then Go Back to Cart Page
	Then Click Add To Cart Button
	Then Click Cart Icon
	Then Click CheckOut
	Then Click Continue As Guest
	Then Verify the User Navigated to Place Order Screen

@web @mobile @demo
Scenario Outline: Successful Login with Valid Credentials
	Given I open the "<site>" website's login page
	Then I Navigate to MyAccount Login Page
	When I log in using Username as "<UserName>" and Password "<Password>" on the login page
	Then I should be logged in
Examples: 
| Description   | site          | UserName                    | Password   |
| Online Portal | Monoprice Web | jayakumart@unitedtechno.com | Welcome@88 |















@web @mobile @monoprice
Scenario Outline: Monoprice Web Login Guest CheckOut
	Given I open the "<site>" website's login page
	Then I Navigate to MyAccount Login Page
	And I Create an account using Username as "<UserName>" and Password "<Password>" 
	And I Click Register Button
	Then I Open the Product Page for the product ID '3992'
	And I change the product quantity to '2' and click AddToCart Button
	Then I Navigate to Shopping Cart Page
	And I Click Proceed To Check Out
	Then I fill all the shipping details
	And Proceed with "<payment method>" as "user" to place an order
	And I Click Place an Order
Examples: 
| Description         | site          | payment method | UserName                                  | Password   |
| Using Mail Payment  | Monoprice Web | MailPayment    |jayakumart+AutomationUser@unitedtechno.com | Welcome@88 |







